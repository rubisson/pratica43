/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Rubinho
 */
public class TrianguloEquilatero extends Retangulo{

    public TrianguloEquilatero(){
    }

    public TrianguloEquilatero(double lado){
        super(lado,lado);
    }    

    @Override
    public double getArea(){
        return (super.getArea()/2);
    }    

    @Override
    public double getPerimetro(){
        return (3*super.getLadoMaior());
    }
}
