/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Rubinho
 */
public class Retangulo implements FiguraComLados{
    private double ladoA;
    private double ladoB;

    public Retangulo(){
    }

    public Retangulo(double ladoA, double ladoB){
        this.ladoA = ladoA;
        this.ladoB = ladoB;
    }

    @Override
    public double getLadoMenor() {
        if (ladoA<ladoB) return ladoA; else return ladoB;
    }
    
    @Override
    public double getLadoMaior() {
        if (ladoA>ladoB) return ladoA; else return ladoB;
    }

    @Override
    public double getPerimetro() {
        return 2*(ladoA+ladoB);
    }

    @Override
    public double getArea() {
        return (ladoA*ladoB);
    }
}
