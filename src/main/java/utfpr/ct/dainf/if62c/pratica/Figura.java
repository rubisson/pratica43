/**
 * Programação em Java.
 * @author Rubisson D. Lamperti
 */

package utfpr.ct.dainf.if62c.pratica;


public interface Figura {
    public double getPerimetro();
    public double getArea();
}
