/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */

//import utfpr.ct.dainf.if62c.pratica.Elipse;
//import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

public class Pratica43 {
    public static void main(String[] args) {
        Retangulo r=new Retangulo(1,2);
        Quadrado q=new Quadrado(2);
        TrianguloEquilatero te=new TrianguloEquilatero(3);
  
        System.out.println("Área do Retangulo é " + r.getArea());
        System.out.println("Perimetro do Retangulo é " + r.getPerimetro());
        System.out.println("Área do Quadrado é " + q.getArea());
        System.out.println("Perimetro do Quadrado é " + q.getPerimetro());
        System.out.println("Área do Triângulo Equilátero é " + te.getArea());
        System.out.println("Perimetro do Triângulo Equilátero é " + te.getPerimetro());

    }
}
